        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-sm navbar-light bg-light shadow">
            <div class="container">
                <a class="navbar-brand" href="{{ url('login') }}" title="Содружество независимых товарищей «Слово Купеческое» имени Спиридона Тримифунтского">
                    <img src="{{ asset('images/logo.png') }}" height="30" class="d-inline-block align-top" alt="">
                    <span class="text-danger">«Слово Купеческое»</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <!-- <a class="nav-link text-danger" href="{{ url('login') }}" title="Содружество независимых товарищей «Слово Купеческое» имени Спиридона Тримифунтского">
                                <i class="far fa-file-alt fa-fw"></i> Устав
                            </a> -->
                        </li>
                    </ul>


                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="{{ url('login') }}" v-on:click.prevent="showMembershipRequestForm = !showMembershipRequestForm, showLoginForm = false">
                                <i class="far fa-handshake fa-fw"></i> Вступить
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-danger" href="{{ url('login') }}" v-on:click.prevent="showLoginForm = !showLoginForm, showMembershipRequestForm = false">
                                <i class="fas fa-sign-in-alt fa-fw"></i> Вход
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
