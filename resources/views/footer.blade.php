    <div class="promo">
        <div class="container">
            <div class="row mb-3">
                <video class="col-12 col-md-8 col-lg-6 col-centered" controls>
                    <source src="{{ asset('/video/Taras-Bulba.mp4') }}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
        </div>
    </div>
    <footer class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <span class="text-muted">&copy; {{ '2019' == date('Y') ? date('Y') : '2019-' . date('Y') }} Содружество независимых товарищей <span class="text-danger">«Слово Купеческое»</span> имени Спиридона Тримифунтского</span>
                </div>
            </div>
        </div>
    </footer>
