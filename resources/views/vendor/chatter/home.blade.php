@extends(Config::get('chatter.master_file_extend'))

@section(Config::get('chatter.yields.head'))
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
    @include('chatter::partials.header.styles')
@stop

@section('content')

<div id="chatter" class="chatter_home">


    @include('chatter::partials.alert')

	<div class="container chatter_container">

	    <div class="row">

	    	<div class="col-3 left-column">
                @include('chatter::partials.sidebar')
	    	</div>
	        <div class="col-9 right-column">
	        	<div class="panel">
		        	<ul class="discussions">
		        		@foreach($discussions as $discussion)
                            @include('chatter::partials.components.discussion')
			        	@endforeach
		        	</ul>
	        	</div>

	        	<div id="pagination">
	        		{{ $discussions->links() }}
	        	</div>

	        </div>
	    </div>
	</div>

	<div id="new_discussion">


    	<div class="chatter_loader dark" id="new_discussion_loader">
		    <div></div>
		</div>

    	<form id="chatter_form_editor" action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}" method="POST">
            @include('chatter::partials.editor.new-discussion')
        </form>

    </div><!-- #new_discussion -->
    
        <form action="{{ url('/adminpanel/forums/category/create') }}" method="POST">
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel">Создать новую категорию</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ csrf_field() }}
                            <input type="text" name="newCategoryName" class="form-control" placeholder="Введите наименование новой категории">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-outline-primary">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>


</div>

@include('chatter::partials.editor.tinymce-config')

@endsection

@section(Config::get('chatter.yields.footer'))
    @include('chatter::partials.footer.scripts-home')
@stop
