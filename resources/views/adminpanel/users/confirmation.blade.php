@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="fas fa-user-times fa-fw"></i> Удаление пользователя
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/users') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>


    <div class="row mb-4">
        <div class="col">

            <!-- <div class="card">
                <div class="card-header">
                    Параметры пользователя
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <dl class="row">
                                <dt class="col-sm-3">№:</dt>
                                <dd class="col-sm-9">{{ $user->apartment }}</dd>

                                <dt class="col-sm-3">Имя:</dt>
                                <dd class="col-sm-9">{{ $user->name }}</dd>

                                <dt class="col-sm-3">E-Mail:</dt>
                                <dd class="col-sm-9">{{ $user->email }}</dd>

                                <dt class="col-sm-3">Статус:</dt>
                                <dd class="col-sm-9">{{ $user->admin ? 'Администратор' : 'Пользователь' }}</dd>
                            </dl>
                        </div>
                        <div class="col-6">
                            <dl class="row">
                                <dt class="col-4">Город:</dt>
                                <dd class="col-8">{{ $user->city }}</dd>

                                <dt class="col-4">Пол:</dt>
                                <dd class="col-8">{{ $user->gender }}</dd>

                                <dt class="col-4">Год рожд.:</dt>
                                <dd class="col-8">{{ $user->year }}</dd>
                            </dl>
                        </div>
                    </div>

                    <span class="text-danger">Внимание!</span> Вы уверены в том, что хотите удалить этого пользователя?
                </div>

                <div class="card-footer text-center">
                    <form>
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-outline-danger shadow" formaction="{{ url('/adminpanel/user/' . $user->id . '/destroy') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Удалить пользователя">
                            <i class="fas fa-times fa-fw"></i> Удалить
                        </button>
                        <a type="submit" class="btn btn-outline-primary shadow" href="{{ url('/adminpanel/users') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                            <i class="fas fa-long-arrow-alt-left fa-fw"></i> Отменить
                        </a>
                    </form>
                </div>
            </div> -->


            <div class="card mb-3" id="avatar">
                <div class="row no-gutters">
                    <div class="col-12 col-md-3 popup-menu px-1 py-1">
                        <!-- Subsequent div's background property contains user's avatar -->
                        <div class="img-thumbnail" v-bind:style="imageUrl" style="width: 100%; height: 100%; min-height: 8rem;"></div>
                    </div>

                    <div class="col-12 col-md-9">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <dl class="row">
                                        <dt class="col-4">№:</dt>
                                        <dd class="col-8">{{ $user->apartment }}</dd>

                                        <dt class="col-4">Имя:</dt>
                                        <dd class="col-8">{{ $user->name }}</dd>

                                        <dt class="col-4">E-Mail:</dt>
                                        <dd class="col-8 text-primary">{{ $user->email }}</dd>

                                        <dt class="col-4">Статус:</dt>
                                        <dd class="col-8 font-italic {{ 1 == $user->id ? 'text-danger' : 'text-secondary' }}">{{ $user->admin ? 'Администратор' : 'Пользователь' }}</dd>
                                    </dl>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <dl class="row">
                                        <dt class="col-4">Город:</dt>
                                        <dd class="col-8">{{ $user->city }}</dd>

                                        <dt class="col-4">Пол:</dt>
                                        <dd class="col-8">{{ $user->gender }}</dd>

                                        <dt class="col-4">Год рожд.:</dt>
                                        <dd class="col-8">{{ $user->year }}</dd>
                                    </dl>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col">
                                    <span class="font-weight-bold">О себе:</span><br>
                                    {{ $user->aboutme }}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <hr class="mt-4">

                <div class="row">
                    <div class="col text-center">
                        <span class="text-danger">Внимание!</span> Вы уверены в том, что хотите удалить этого пользователя?
                    </div>
                </div>

                <div class="card-footer text-center mt-3">
                    <form>
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-outline-danger shadow" formaction="{{ url('/adminpanel/user/' . $user->id . '/destroy') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Удалить пользователя">
                            <i class="fas fa-times fa-fw"></i> Удалить
                        </button>
                        <a type="submit" class="btn btn-outline-primary shadow" href="{{ url('/adminpanel/users') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                            <i class="fas fa-long-arrow-alt-left fa-fw"></i> Отменить
                        </a>
                    </form>
                </div>
            </div>


        </div>
    </div>
@endsection


@section('customJs')
    <script>
        var avatar = new Vue({
            el: '#avatar',

            data: {
                imageUrl: {
                    background:  "url('/adminpanel/avatar/get/{{ $user->id }}') 50% no-repeat",
                    backgroundSize: 'contain',
                    backgroundPosition: 'top'
                }
            }
        });
    </script>
@endsection
