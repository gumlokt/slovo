@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="fas fa-users fa-fw"></i> Управление пользователями
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            @if (1 == Auth::user()->id)
                <form>
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/create') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Создать нового пользователя">
                        <i class="fas fa-user-plus fa-fw"></i>
                    </button>
                </form>
            @endif
        </div>
    </div>






    <div class="row">
        <div class="col">


            <table class="table table-hover">
                <thead>
                    <tr class="table-info">
                        <th scope="col">
                            <i class="fas fa-user" data-toggle="tooltip" data-placement="top" data-html="true" title="<i class='fas fa-user-secret'></i> - Администратор<br><i class='far fa-user'></i> - Пользователь&nbsp;&nbsp;&nbsp;"></i>
                        </th>
                        <th scope="col">№</th>
                        <!-- <th scope="col">Статус</th> -->
                        <th scope="col">Имя</th>
                        <th scope="col">Возраст</th>
                        <th scope="col">Пол</th>
                        <th scope="col">Город</th>
                        <th scope="col">Почта</th>
                        <th scope="col" data-toggle="tooltip" data-placement="top" title="ГГГГ-ММ-ДД ЧЧ:ММ:СС">Активность</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr class="{{ 1 != $user->id ? $user->active ? 'table-warning' : 'active' : 'table-danger' }}">
                            <td>
                                @if ($user->admin)
                                    <i class="fas fa-user-secret{{ $user->isOnline() ? ' text-success' : '' }}"></i>
                                @else
                                    <i class="far fa-user{{ $user->isOnline() ? ' text-success' : '' }}"></i>
                                @endif
                            </td>

                            <td>{{ $user->apartment }}</td>

                            <!-- <td>
                                @if($user->isOnline())
                                    <i class="fas fa-circle text-success fa-fw fa-sm"></i>
                                @endif
                            </td> -->

                            <td>
                                <a data-toggle="collapse" href="#collapseAboutUser{{ $user->id }}" role="button" aria-expanded="false" aria-controls="collapseAboutUser{{ $user->id }}">
                                    {{ $user->name }} {!! $user->aboutme ? '<i class="fas fa-caret-down fa-fw"></i>' : ''!!}
                                </a>
                                <div class="collapse" id="collapseAboutUser{{ $user->id }}">
                                    {{ $user->aboutme }}
                                </div>
                            </td>

                            <td>{{ $user->year }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->city }}</td>

                            <td>{{ $user->email }}</td>

                            <td class="{{ 1 == Auth::user()->id ? 'popup-menu' : ''}}{{ $user->isOnline() ? ' text-success' : '' }}">
                                {{ $user->last }}

                                @if (1 == Auth::user()->id)
                                    <form class="form-inline">
                                        @if (1 == $user->id or Auth::user()->id == $user->id)
                                            @if ($user->active)
                                                <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/reinvite') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Отправить приглашение повторно" disabled><i class="far fa-paper-plane fa-fw"></i></button>
                                                <button class="btn btn-success btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/pause') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Деактивировать" disabled><i class="fas fa-pause fa-fw"></i></button>
                                            @else
                                                <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/activate') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Активировать" disabled><i class="fas fa-play fa-fw"></i></button>
                                            @endif

                                            <button class="btn btn-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Редактировать" disabled><i class="fa fa-pencil-alt fa-fw"></i></button>
                                            <button class="btn btn-danger btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/delete') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Удалить" disabled><i class="fa fa-times fa-fw"></i></button>
                                        @else
                                            @if ($user->active)
                                                <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/reinvite') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Отправить приглашение повторно"><i class="far fa-paper-plane fa-fw"></i></button>
                                                <button class="btn btn-success btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/pause') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Деактивировать"><i class="fas fa-pause fa-fw"></i></button>
                                            @else
                                                <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/activate') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Активировать"><i class="fas fa-play fa-fw"></i></button>
                                            @endif

                                            <button class="btn btn-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Редактировать"><i class="fa fa-pencil-alt fa-fw"></i></button>
                                            <button class="btn btn-danger btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/user/' . $user->id . '/delete') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Удалить"><i class="fa fa-times fa-fw"></i></button>
                                        @endif
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <blockquote class="blockquote text-right">
                Всего пользователей: <strong class="">{{ $users->count() }}</strong><br>
                <!-- <small><em class="text-muted">Из них на сайте: <strong class="text-success">1</strong></em></small> -->
            </blockquote>




        </div>
    </div>
@endsection
