@extends("adminpanel.layouts.app")

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="fas fa-users fa-fw"></i> Пользователи
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                @if (isset($user) and str_contains(url()->current(), 'profile'))
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/profile') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                    </button>
                @else
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/users') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                    </button>
                @endif
            </form>
        </div>
    </div>






    <div class="row mb-4">
        <div class="col">
            <form class="card">
                {{ csrf_field() }}

                <h5 class="card-header popup-menu">
                    @if (isset($user))
                        @if (str_contains(url()->current(), 'profile'))
                            <i class="fas fa-user-edit fa-fw"></i> Редактирование профиля
                        @else
                            <i class="fas fa-user-edit fa-fw"></i> Редактирование пользователя
                        @endif
                    @else
                        <i class="fas fa-user-plus fa-fw"></i> Создать нового пользователя
                    @endif
                </h5>

                <div class="card-body">
                    <h5 class="text-muted">Обязательные поля:</h5>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group row">
                                <label for="apartment" class="col-12 col-lg-4 col-form-label">Порядковый номер:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="text" name="apartment" class="form-control" placeholder="Порядковый номер" value="{{ isset($user) ? $user->apartment : old('apartment') }}" id="apartment">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-12 col-lg-4 col-form-label">Имя:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="text" name="name" class="form-control" placeholder="Имя пользователя" value="{{ isset($user) ? $user->name : old('name') }}" id="name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-12 col-lg-4 col-form-label">Привилегии:</label>
                                <div class="col-12 col-lg-8">
                                    <select name="admin" class="form-control" id="admin">
                                        @if (isset($user))
                                            @if (str_contains(url()->current(), 'profile'))
                                                @if ($user->admin)
                                                    <option value="1" selected>Администратор</option>
                                                @else
                                                    <option value="0" selected>Пользователь</option>
                                                @endif
                                            @else
                                                <option value="0"{{ $user->admin ? '' : ' selected' }}>Пользователь</option>
                                                <option value="1"{{ $user->admin ? ' selected' : '' }}>Администратор</option>
                                            @endif
                                        @else
                                            <option value="0" selected>Пользователь</option>
                                            <option value="1">Администратор</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group row">
                                <label for="email" class="col-12 col-lg-4 col-form-label">E-Mail:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="email" name="email" class="form-control" placeholder="E-Mail пользователя" value="{{ isset($user) ? $user->email : old('email') }}" id="email">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-12 col-lg-4 col-form-label">Пароль:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="password" name="password" class="form-control" placeholder="Пароль пользователя" id="password">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-12 col-lg-4 col-form-label">Подтверждение:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="Подтверждение пароля" id="password-confirm">
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="dropdown-divider"></div>
                    <h5 class="text-muted">Не обязательные поля:</h5>

                    <div class="row pt-2">
                        <div class="col-12 col-md-6">
                            <div class="form-group row">
                                <label for="city" class="col-12 col-lg-4 col-form-label">Город:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="text" name="city" class="form-control" placeholder="Город" value="{{ isset($user) ? $user->city : old('city') }}" id="city">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="gender" class="col-12 col-lg-4 col-form-label">Пол:</label>
                                <div class="col-12 col-lg-8">
                                    <select name="gender" class="custom-select" id="gender">
                                        <option value="" {{ isset($user) ? '' == $user->gender ? ' selected': '' : old('gender') }}>Не указано</option>
                                        <option value="Муж." {{ isset($user) ? 'Муж.' == $user->gender ? ' selected': '' : old('gender') }}>Мужчина</option>
                                        <option value="Жен." {{ isset($user) ? 'Жен.' == $user->gender ? ' selected': '' : old('gender') }}>Женщина</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="year" class="col-12 col-lg-4 col-form-label">Год рождения:</label>
                                <div class="col-12 col-lg-8">
                                    <input type="text" name="year" class="form-control" placeholder="Год рождения" value="{{ isset($user) ? $user->year : old('year') }}" id="year">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group row">
                                <label for="aboutme" class="col-12 col-lg-4 col-form-label">О себе:</label>
                                <div class="col-12 col-lg-8">
                                    <textarea name="aboutme" class="form-control" placeholder="О себе" rows="5" id="aboutme">{{ isset($user) ? $user->aboutme : old('aboutme') }}</textarea>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <div class="card-footer text-center">
                    @if (isset($user))
                        @if (str_contains(url()->current(), 'profile'))
                            <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/profile/' . $user->id . '/update') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Сохранить изменения">
                                <i class="far fa-save fa-fw"></i> Сохранить
                            </button>
                        @else
                            <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/user/' . $user->id . '/update') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Сохранить изменения">
                                <i class="far fa-save fa-fw"></i> Сохранить
                            </button>
                        @endif
                    @else
                        <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/user/store') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Зарегистрировать нового пользователя">
                            <i class="fas fa-check fa-fw"></i> Создать
                        </button>
                    @endif

                    @if (str_contains(url()->current(), 'profile'))
                        <a type="submit" class="btn btn-outline-danger shadow" href="{{ url('/adminpanel/profile') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                            <i class="fas fa-times fa-fw"></i> Отменить
                        </a>
                    @else
                        <a type="submit" class="btn btn-outline-danger shadow" href="{{ url('/adminpanel/users') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                            <i class="fas fa-times fa-fw"></i> Отменить
                        </a>
                    @endif
                </div>

            </form>
        </div>
    </div>

    <div class="row">
        <div class="col">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if (session('message'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {!! session('message') !!}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
@endsection
