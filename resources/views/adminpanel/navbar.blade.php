        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-xl navbar-light bg-light shadow">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#adminNavbar" aria-controls="adminNavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="adminNavbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item{{ ends_with(url()->current(), 'adminpanel') ? ' active' : str_contains(url()->current(), 'post') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel') }}">
                                <i class="far fa-newspaper fa-fw"></i> Новости
                            </a>
                        </li>

                        <li class="nav-item{{ ends_with(url()->current(), 'documents') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/documents') }}">
                                <i class="far fa-file-alt fa-fw"></i> Документация
                            </a>
                        </li>

                        <li class="nav-item{{ ends_with(url()->current(), 'forums') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/forums') }}">
                                <i class="far fa-comments fa-fw"></i> Форум
                            </a>
                        </li>

                        <li class="nav-item{{ str_contains(url()->current(), 'poll') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/polls') }}">
                                <i class="fas fa-chart-pie fa-fw"></i> Опросы
                            </a>
                        </li>

                        @if (1 == Auth::user()->id)
                            <li class="nav-item{{ str_contains(url()->current(), 'mailings') ? ' active' : str_contains(url()->current(), 'message') ? ' active' : '' }}">
                                <a class="nav-link" href="{{ url('adminpanel/mailings') }}">
                                    <i class="far fa-envelope fa-fw"></i> Рассылка
                                </a>
                            </li>
                        @endif
                    </ul>


                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item{{ str_contains(url()->current(), 'conversations') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/conversations') }}">
                                <i class="far fa-paper-plane fa-fw"></i> Беседка
                            </a>
                        </li>

                        <li class="nav-item{{ str_contains(url()->current(), 'user') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/users') }}">
                                <i class="fas fa-users fa-fw"></i> Пользователи
                            </a>
                        </li>

                        <li class="nav-item{{ str_contains(url()->current(), 'profile') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('adminpanel/profile') }}">
                                <i class="far fa-id-card fa-fw"></i> Профиль
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt fa-fw"></i> Выход
                            </a>
                            <form id="  -form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
