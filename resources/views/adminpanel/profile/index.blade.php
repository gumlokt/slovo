@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="far fa-id-card fa-fw"></i> Мой профиль
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            <form>
                <button class="btn btn-outline-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/profile/' . Auth::user()->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Редактировать профиль">
                    <i class="fas fa-user-edit"></i>
                </button>
            </form>
        </div>
    </div>



    <div class="row">
        <div class="col">

            <div class="card mb-3" id="avatar">
                <div class="row no-gutters">
                    <div class="col-12 col-md-3 popup-menu px-1 py-1">
                        <!-- Subsequent div's background property contains user's avatar -->
                        <div class="img-thumbnail" v-bind:style="imageUrl" style="width: 100%; height: 100%; min-height: 8rem;">
                            <div class="form-inline" style="left: 5px;">
                                <button class="btn btn-warning btn-sm shadow" v-on:click="showFileBrowseBtn" data-toggle="tooltip" data-placement="top" title="Прикрепить изображение"><i class="fas fa-pencil-alt fa-fw"></i></button>
                                <button class="btn btn-danger btn-sm shadow" v-on:click="deleteImage" data-toggle="tooltip" data-placement="top" title="Удалить изображение"><i class="fas fa-times fa-fw"></i></button>

                                <input type="file" v-on:change="changeImage" v-show="showFileBrowse" name="image">
                                <input v-if="selectedImage" v-model="selectedImage.name" type="hidden" name="avatar" />
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-9">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <dl class="row">
                                        <dt class="col-4">№:</dt>
                                        <dd class="col-8">{{ Auth::user()->apartment }}</dd>

                                        <dt class="col-4">Имя:</dt>
                                        <dd class="col-8">{{ Auth::user()->name }}</dd>

                                        <dt class="col-4">E-Mail:</dt>
                                        <dd class="col-8 text-primary">{{ Auth::user()->email }}</dd>

                                        <dt class="col-4">Статус:</dt>
                                        <dd class="col-8 font-italic {{ 1 == Auth::user()->id ? 'text-danger' : 'text-secondary' }}">{{ Auth::user()->admin ? 'Администратор' : 'Пользователь' }}</dd>
                                    </dl>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <dl class="row">
                                        <dt class="col-4">Город:</dt>
                                        <dd class="col-8">{{ Auth::user()->city }}</dd>

                                        <dt class="col-4">Пол:</dt>
                                        <dd class="col-8">{{ Auth::user()->gender }}</dd>

                                        <dt class="col-4">Год рожд.:</dt>
                                        <dd class="col-8">{{ Auth::user()->year }}</dd>
                                    </dl>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col">
                                    <span class="font-weight-bold">О себе:</span><br>
                                    {{ Auth::user()->aboutme }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('customJs')
    <script>
        var avatar = new Vue({
            el: '#avatar',

            data: {
                imageUrl: {
                    background:  "url('/adminpanel/avatar/get/{{ Auth::user()->id }}') 50% no-repeat",
                    backgroundSize: 'contain',
                    backgroundPosition: 'top'
                },

                showFileBrowse: false,
                selectedImage: null,
                allowableTypes: ['jpg', 'jpeg', 'png', 'gif', 'webp'],
                maximumSize: 100000
            },

            methods: {
                showFileBrowseBtn: function () {
                    this.showFileBrowse = !this.showFileBrowse;
                },

                changeImage(e) {
                    let files = e.target.files || e.dataTransfer.files;

                    if (!files.length) {
                        return;
                    }

                    this.selectedImage = files[0];

                    if (!this.validate(this.selectedImage)) {
                        return;
                    }

                    // create a form
                    const form = new FormData();
                    form.append('avatar', this.selectedImage);
                    axios.post('/adminpanel/avatar/set', form)
                        .then((response) => {
                            // console.log(response);
                            var image = new Image();
                            var reader = new FileReader();
                            var vm = this;

                            reader.onload = (e) => {
                                vm.imageUrl.background = "url('" + e.target.result + "') 50% no-repeat";
                            };
                            reader.readAsDataURL(this.selectedImage);
                        });

                    this.showFileBrowse = !this.showFileBrowse;
                },

                validate(image) {
                    if (!this.allowableTypes.includes(image.name.split(".").pop().toLowerCase())) {
                        alert(`Формат выбранного вами файла не относится к числу разрешенных для загрузки. Разрешены только эти типы изображений: ${this.allowableTypes.join(", ").toUpperCase()}.`)
                        return false
                    }

                    if (image.size > this.maximumSize){
                        alert("Выбранное вами изображение слишком большое. Максимально допустимый размер: 100 Кб.")
                        return false
                    }

                    return true
                },

                deleteImage: function () {
                    axios.get('/adminpanel/avatar/delete/{{ Auth::user()->id }}')
                        .then((response) => {
                            // handle success
                            this.imageUrl.background = "url('/adminpanel/avatar/get/default.png') 50% no-repeat";
                            // console.log(response.data);
                        }).catch((error) => {
                            // handle error
                            console.log(error);
                        });

                    this.showFileBrowse = false;
                }
            }
        });
    </script>
@endsection
