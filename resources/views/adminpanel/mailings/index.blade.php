@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="far fa-envelope fa-fw"></i> Отправленные сообщения
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            @if (1 == Auth::user()->id)
                <form>
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/letter/create') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Создать новое сообщение">
                        <i class="fas fa-plus fa-fw"></i>
                    </button>
                </form>
            @endif
        </div>
    </div>







    <div class="row">
        <div class="col">

            @if (isset($letters[0]))
                <table class="table table-hover">
                    <thead>
                        <tr class="table-primary">
                            <th scope="col">Адресаты</th>
                            <th scope="col">Сообщение</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($letters as $letter)
                            <tr class="table-warning">
                                <td width="25%">
                                    @foreach ($letter->mailings as $mailing)
                                        @if (5 == $loop->index)
                                            <a class="" data-toggle="collapse" href="#collapseEmails{{ $letter->id }}" role="button" aria-expanded="false" aria-controls="collapseEmails{{ $letter->id }}">
                                                <i class="fas fa-caret-down"></i> Остальные...
                                            </a>

                                            <div class="collapse" id="collapseEmails{{ $letter->id }}">
                                        @endif
                                        
                                        @if (isset($emails[$mailing->user_id]))
                                            {{ $emails[$mailing->user_id] }}<br>
                                        @endif
                                    @endforeach

                                        @if (count($letter->mailings) > 5)
                                            </div>
                                        @endif
                                </td>

                                <td>
                                    <span class="text-info">Отправлено: {{ $letter->created_at }}</span><br>
                                    <strong>{{ $letter->subject }}</strong><br>
                                    {{ $letter->letter }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

        </div>
    </div>
@endsection
