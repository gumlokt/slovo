@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="far fa-newspaper fa-fw"></i> Новости
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            @if (1 == Auth::user()->id)
                <form>
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/post/create') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Создать новую статью">
                        <i class="fas fa-plus fa-fw"></i>
                    </button>
                </form>
            @endif
        </div>
    </div>


    @if (isset($posts[0]))
        @foreach ($posts as $post)
            <div class="row mb-4">
                <div class="col">
                    <div class="card">

                        <h5 class="card-header popup-menu">
                            <div class="row">
                                <div class="col-8">
                                    <a href="{{ url('/adminpanel/post/' . $post->id . '/show') }}">
                                        <i class="fas fa-minus fa-fw fa-xs"></i> {{ $post->title }}
                                    </a>
                                </div>

                                <div class="col-4 text-right">
                                    <small class="text-muted"><em>{{ date_format($post->created_at, 'd.m.Y H:i') }}</em></small>
                                </div>
                            </div>

                            @if (1 == Auth::user()->id or Auth::user()->id == $post->user_id)
                                <form class="form-inline">
                                    <button class="btn btn-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/post/' . $post->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Редактировать">
                                        <i class="fa fa-pencil-alt fa-fw"></i>
                                    </button>
                                    <button class="btn btn-danger btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/post/' . $post->id . '/delete') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Удалить">
                                        <i class="fa fa-times fa-fw"></i>
                                    </button>
                                </form>
                            @endif
                        </h5>

                        <div class="card-body">
                            {!! strip_tags(substr($post->post, 0, 800)) !!}...<br>
                            <a href="{{ url('/adminpanel/post/' . $post->id . '/show') }}">
                                Подробнее <i class="fas fa-angle-double-right fa-fw fa-xs"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    @endif

    <div class="row mb-4">
        <div class="col">
        </div>
    </div>
@endsection
