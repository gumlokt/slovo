@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="fas fa-chart-pie fa-fw"></i> Результаты опроса
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/polls') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> К действующим опросам
                </button>
            </form>
        </div>
    </div>



    @if (isset($poll))
        <div class="row mb-4">
            <div class="col">
                <div class="card">

                    <h5 class="card-header popup-menu">
                        <div class="row">
                            <div class="col-8">
                                <i class="fas fa-minus fa-fw fa-xs"></i> {{ $poll->poll }}
                            </div>

                            <div class="col-4 text-right">
                                <small class="text-muted"><em>{{ date_format($poll->created_at, 'd.m.Y H:i') }}</em></small>
                            </div>
                        </div>

                        @if (1 == Auth::user()->id or Auth::user()->id == $poll->user_id)
                            <form class="form-inline">
                                @if ($poll->active)
                                    <button class="btn btn-success btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/pause') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Приостановить опрос"><i class="fas fa-pause fa-fw"></i></button>
                                @else
                                    <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/activate') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Возобновить опрос"><i class="fas fa-play fa-fw"></i></button>
                                @endif

                                <button class="btn btn-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-pencil-alt fa-fw"></i></button>
                                <button class="btn btn-info btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/archive') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Поместить в архив"><i class="fas fa-level-down-alt fa-fw"></i></button>
                                <button class="btn btn-danger btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/delete') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Удалить"><i class="fa fa-times fa-fw"></i></button>
                            </form>
                        @endif
                    </h5>

                    <div class="card-body">
                        @foreach ($poll->answers as $answer)
                            <h4>
                                {{ $answer->answer }}

                                @if ($answer->id == $myChoice)
                                    <span class="text-danger" data-toggle="tooltip" data-placement="top" title="Мой выбор"><i class="fas fa-asterisk fa-fw fa-sm"></i></span>
                                @endif

                                @if ($results[$answer->id])
                                    <em><small>(число голосов: <strong class="text-danger">{{ $results[$answer->id] }}</strong>)</small></em>
                                @endif
                            </h4>
                            <div class="progress mb-3">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ array_sum($results) ? round(100 * $results[$answer->id]/array_sum($results)) : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ array_sum($results) ? round(100 * $results[$answer->id]/array_sum($results)) : 0 }}%; min-width: 2em;">
                                    {{ array_sum($results) ? round(100 * $results[$answer->id]/array_sum($results)) : 0 }}%
                                </div>
                            </div>
                        @endforeach
                        <h5>
                            <em>Общее количество проголосовавших: <strong class="text-danger">{{ array_sum($results) }}</strong></em>
                        </h5>

                        <hr>
                        <a href="{{ $poll->archive ? url('/adminpanel/polls/archives') : url('/adminpanel/polls') }}"><i class="fas fa-angle-double-left fa-fw fa-sm"></i> Вернуться назад</a>
                    </div>

                </div>
            </div>
        </div>
    @endif
@endsection
