@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="fas fa-chart-pie fa-fw"></i> Действующие опросы
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            @if (1 == Auth::user()->id)
                <form>
                    <button class="btn btn-outline-info btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/polls/archives') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Архивные опросы">
                        <i class="fas fa-archive fa-fw"></i>
                    </button>
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/create') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Создать новый опрос">
                        <i class="fas fa-plus fa-fw"></i>
                    </button>
                </form>
            @endif
        </div>
    </div>




    @if (isset($polls))
        @foreach ($polls as $poll)
        <div class="row mb-4">
                <div class="col">
                    <div class="card">

                        <h5 class="card-header popup-menu">
                            <div class="row">
                                <div class="col-8">
                                    <i class="fas fa-minus fa-fw fa-xs"></i> {{ $poll->poll }}
                                </div>

                                <div class="col-4 text-right">
                                    <small class="text-muted"><em>{{ date_format($poll->created_at, 'd.m.Y H:i') }}</em></small>
                                </div>
                            </div>

                            @if (1 == Auth::user()->id or Auth::user()->id == $poll->user_id)
                                <form class="form-inline">
                                    @if ($poll->active)
                                        <button class="btn btn-success btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/pause') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Приостановить опрос"><i class="fas fa-pause fa-fw"></i></button>
                                    @else
                                        <button class="btn btn-secondary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/activate') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Возобновить опрос"><i class="fas fa-play fa-fw"></i></button>
                                    @endif

                                    <button class="btn btn-warning btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/edit') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-pencil-alt fa-fw"></i></button>
                                    <button class="btn btn-info btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/archive') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Поместить в архив"><i class="fas fa-level-down-alt fa-fw"></i></button>
                                    <button class="btn btn-danger btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/delete') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Удалить"><i class="fa fa-times fa-fw"></i></button>
                                </form>
                            @endif
                        </h5>

                        <div class="card-body">
                            <form action="{{ url('/adminpanel/poll/' . $poll->id . '/vote') }}" method="POST">
                                {{ csrf_field() }}

                                @foreach ($poll->answers as $answer)
                                    <div class="custom-control custom-radio mb-1">
                                        <input type="radio" id="{{ $answer->id }}" name="choice" value="{{ $answer->id }}" class="custom-control-input"{{ (isset($choices[$poll->id]) and $choices[$poll->id] == $answer->id) ? ' checked' : '' }}{{ $poll->active ? '' : ' disabled' }}>
                                        <label class="custom-control-label" for="{{ $answer->id }}">
                                            @if (isset($choices[$poll->id]) and $choices[$poll->id] == $answer->id)
                                                <strong>{{ $answer->answer }}</strong> <span class="text-danger" data-toggle="tooltip" data-placement="top" title="Мой выбор"><i class="fas fa-asterisk fa-fw"></i></span>
                                            @else
                                                {{ $answer->answer }}
                                            @endif
                                        </label>
                                    </div>
                                @endforeach

                                @if (isset($choices[$poll->id]))
                                    <button class="btn btn-outline-secondary btn-sm shadow{{ $poll->active ? '' : ' disabled' }}" type="submit"{{ $poll->active ? '' : ' disabled' }}>
                                        <i class="fas fa-check fa-fw"></i> Изменить выбор
                                    </button>
                                    <a href="{{ url('/adminpanel/poll/' . $poll->id . '/voteRemove') }}" class="btn btn-outline-secondary btn-sm shadow{{ $poll->active ? '' : ' disabled' }}" type="submit"{{ $poll->active ? '' : ' disabled' }}>
                                        <i class="fa fa-times fa-fw"></i> Убрать мой голос из опроса
                                    </a>
                                @else
                                    <button class="btn btn-outline-secondary btn-sm shadow{{ $poll->active ? '' : ' disabled' }}" type="submit"{{ $poll->active ? '' : ' disabled' }}>
                                        <i class="fas fa-check fa-fw"></i> Голосовать
                                    </button>
                                @endif
                            </form>


                            <div class="row border-top mt-3 pt-3">
                                <div class="col">
                                    <a href="{{ url('/adminpanel/poll/' . $poll->id . '/show') }}">
                                        Результаты <i class="fas fa-angle-double-right fa-fw fa-xs"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach

        {{ $polls->links() }}
        <br>

        @if (!isset($polls[0]))
            <p>На данный момент действующие опросы отсутствуют...</p>
        @endif
    @endif
@endsection
