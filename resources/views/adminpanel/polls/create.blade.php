@extends("adminpanel.layouts.app")

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                @if (isset($poll))
                    <i class="fa fa-pencil-alt fa-fw"></i> Редактирование опроса
                @else
                    <i class="far fa-file fa-fw"></i> Новый опрос
                @endif
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                @if (isset($poll))
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ $poll->archive ? url('/adminpanel/polls/archives') : url('/adminpanel/polls') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                    </button>
                @else
                    <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/polls') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" data-html="true" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                    </button>
                @endif
            </form>
        </div>
    </div>






    <div class="row">
        <div class="col">


            <form>
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="poll">Название опроса:</label>
                    <input type="text" name="poll" class="form-control" placeholder="Название опроса" value="{{ isset($poll) ? $poll->poll : old('poll') }}" id="poll">
                </div>


                <div class="form-group" id="vue">
                    <label for="answer">Варианты ответа:</label>
                    <div class="col-11 offset-sm-1" id="answers">
                        @if (isset($answers))
                            @for ($i = 0; $i < count($answers); $i++)
                                <div class="row mb-2">
                                    <div class="col-10">
                                        <input type="text" name="answers[]" class="form-control" value="{{ $answers[$i]->answer}}" placeholder="Вариант ответа">
                                    </div>
                                    @if ($i > 1)
                                        <button class="btn btn-outline-danger btn-sm shadow" onclick="$($(this).parent()).remove()" data-toggle="tooltip" data-placement="top" title="Удалить этот вариант ответа">
                                            <i class="fas fa-minus fa-fw fa-xs"></i>
                                        </button>
                                    @endif
                                </div>
                            @endfor
                        @else
                            <div class="row mb-2">
                                <div class="col-10">
                                    <input type="text" name="answers[]" class="form-control" placeholder="Вариант ответа">
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-10">
                                    <input type="text" name="answers[]" class="form-control" placeholder="Вариант ответа">
                                </div>
                            </div>
                        @endif
                    </div>


                    <div class="col-11 offset-sm-1">
                        <button v-on:click.prevent="addAnswer" class="btn btn-outline-primary btn-sm shadow" data-toggle="tooltip" data-placement="top" title="Добавить вариант ответа" id="addAnswer">
                            <i class="fas fa-plus fa-fw"></i>
                        </button>
                    </div>
                </div>
<hr>
                <div class="form-group text-center">
                    @if (isset($poll))
                        <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/update') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Сохранить изменения">
                            <i class="far fa-save fa-fw"></i> Сохранить
                        </button>
                    @else
                        <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/poll/store') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Создать новый опрос">
                            <i class="fas fa-check fa-fw"></i> Создать
                        </button>
                    @endif

                    <a type="submit" class="btn btn-outline-danger shadow" href="{{ url('/adminpanel/polls') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-times fa-fw"></i> Отменить
                    </a>
                </div>
            </form>
            <br>

            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    @foreach ($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach
                </div>
            @endif

            @if (session('message'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    {!! session('message') !!}
                </div>
            @endif


        </div>
    </div>
@endsection
