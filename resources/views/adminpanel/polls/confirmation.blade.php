@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="fas fa-chart-pie fa-fw"></i> Удаление опроса
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url()->previous() }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернутья назад">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>


    <div class="row mb-4">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    {{ $poll->poll }}
                </div>

                <div class="card-body">
                    <span class="text-danger">Внимание!</span> Вы уверены в том, что хотите удалить этот опрос?
                </div>

                <div class="card-footer text-center">
                    <form>
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-outline-danger shadow" formaction="{{ url('/adminpanel/poll/' . $poll->id . '/destroy') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Удалить опрос">
                            <i class="fas fa-times fa-fw"></i> Удалить
                        </button>

                        <button class="btn btn-outline-primary shadow" type="submit" formaction="{{ url()->previous() }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернутья назад">
                            <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
