@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="far fa-paper-plane fa-fw"></i> Беседка
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
        </div>
    </div>








            <div class="card">
                <div class="card-header" style="padding: 0;">
                </div>
                <div class="card-body" style="padding: 0 !important;" id="app">
                    <chat-app :user="{{ auth()->user() }}"></chat-app>
                </div>
            </div>
@endsection

@section('customJs')
    <script>
        var app = new Vue({
            el: '#app'
        })
    </script>
@endsection
