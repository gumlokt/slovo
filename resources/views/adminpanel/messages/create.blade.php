@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="far fa-file fa-fw"></i> Новое сообщение
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel/mailings') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>





    <form class="row mb-4" id="sendMailForm">
        {{ csrf_field() }}

        <div class="col-12 mb-4">
            <p>
                <button class="btn btn-outline-dark dropdown-toggle shadow" type="button" data-toggle="collapse" data-target="#collapseEmails" aria-expanded="false" aria-controls="collapseEmails">
                    <span class="caret"></span> Кому:
                </button>

                <em class="text-info" v-if="checkedEmails.length == allUsers.length">
                    Всем <span class="text-muted">(по умолчанию)</span>
                </em>
                <em class="text-info" v-else-if="checkedEmails.length">
                    @{{ checkedEmails }}
                </em>
            </p>

            <div class="collapse" id="collapseEmails">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-outline-secondary btn-sm shadow" v-on:click.prevent="checkAllEmails()">
                            <i class="far fa-check-square fa-fw"></i> Выбрать все
                        </button>
                        <button type="button" class="btn btn-outline-secondary btn-sm shadow" v-on:click.prevent="unCheckAllEmails()">
                            <i class="far fa-square fa-fw"></i> Убрать все
                        </button>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            @if (isset($users) and !empty($users))
                                <template v-for="(user, index) in allUsers">
                                    <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="checkedEmails[]" v-model="checkedEmails" v-bind:value="user.email">
                                                @{{ user.email }}
                                                <em class="text-info">@{{ user.name }}</em>
                                            </label>
                                        </div>
                                    </div>
                                </template>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-12">
            <div class="card">

                <div class="card-header">
                    <div class="div-group row">
                        <label for="subject" class="col-5 col-sm-3 col-lg-2 col-form-label">Тема:</label>
                        <div class="col-7 col-sm-9 col-lg-10">
                            <input type="text" class="form-control"  placeholder="Тема сообщения" name="subject" v-model="subject" value="{{ old('subject') }}" id="subject">
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="subject" class="col-5 col-sm-3 col-lg-2 col-form-label">Сообщение:</label>
                        <div class="col-7 col-sm-9 col-lg-10">
                            <textarea class="form-control" name="message" v-model="message" rows="3" id="message">{{ old('message') }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/message/store') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Отправить сообщение" v-bind:disabled="checkAllFieldsAreFilled">
                        <i class="fa fa-check" aria-hidden="true"></i> Отправить
                    </button>

                    <a type="submit" class="btn btn-outline-danger shadow" href="{{ url('/adminpanel/mailings') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                        <i class="fa fa-remove" aria-hidden="true"></i> Отменить
                    </a>
                </div>

            </div>
        </div>
    </form>

    

    <div class="row mb-4">
        <div class="col">
            @if (count($errors) > 0)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
        
                    @foreach ($errors->all() as $error)
                        {!! $error !!}<br>
                    @endforeach
                </div>
            @endif
        
            @if (session('message'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
        
                    {!! session('message') !!}
                </div>
            @endif
        </div>
    </div>
@endsection


@section('customJs')
    <script>
        var allEmails = [
            @foreach ($users as $user)
                '{{ $user->email }}',
            @endforeach
        ];

        var allUsers = [
            @foreach ($users as $user)
                { 'email': '{{ $user->email }}', 'name': '{{ $user->name }}',},
            @endforeach
        ];

        var recipients = new Vue({
            el: '#sendMailForm',

            data: {
                allUsers: allUsers,
                checkedEmails: allEmails,
                subject: '',
                message: ''
            },

            methods: {
                checkAllEmails: function () {
                    this.checkedEmails = allEmails;
                },
                unCheckAllEmails: function () {
                    this.checkedEmails = [];
                }
            },

            computed: {
                checkAllFieldsAreFilled: function () {
                    if (this.checkedEmails.length && this.subject && this.message) {
                        return false;
                    }
                    return true;
                }
            }
        });
    </script>
@endsection
