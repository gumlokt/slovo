@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="far fa-trash-alt fa-fw"></i> Удаление записи
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернутья назад">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>


    <div class="row mb-4">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    {{ $post->title }}
                </div>

                <div class="card-body">
                    {!! strip_tags(substr($post->post, 0, 800)) !!}
                </div>

                <form class="card-footer text-center">
                    <form>
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-outline-danger shadow" formaction="{{ url('/adminpanel/post/' . $post->id . '/destroy') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Удалить пользователя">
                            <i class="fas fa-times fa-fw"></i> Удалить
                        </button>

                        <a type="submit" class="btn btn-outline-primary shadow" href="{{ url('/adminpanel') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                            <i class="fas fa-long-arrow-alt-left fa-fw"></i> Отменить
                        </a>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
