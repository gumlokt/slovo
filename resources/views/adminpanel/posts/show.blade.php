@extends("adminpanel.layouts.app")

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                <i class="fas fa-circle fa-fw fa-xs"></i> {{ $post->title }}
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url()->previous() }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернутья назад">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>


    @if (isset($post->post))
        <div class="row mb-4">
            <div class="col">
                {!! $post->post !!}
            </div>
        </div>
    @endif
@endsection
