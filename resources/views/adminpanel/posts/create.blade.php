@extends("adminpanel.layouts.app")

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8 col-md-9 col-lg-10">
            <h3 class="text-danger">
                @if (isset($post))
                    <i class="fas fa-pencil-alt fa-fw"></i> Редактирование записи
                @else
                    <i class="far fa-file fa-fw"></i> Добавить новость
                @endif
            </h3>
        </div>

        <div class="col-4 col-sm-4 col-md-3 col-lg-2 text-right">
            <form>
                <button class="btn btn-outline-primary btn-sm shadow" type="submit" formaction="{{ url('/adminpanel') }}" formmethod="GET" data-toggle="tooltip" data-placement="top" title="Вернутья назад">
                    <i class="fas fa-long-arrow-alt-left fa-fw"></i> Назад
                </button>
            </form>
        </div>
    </div>




    <div class="row">
        <div class="col">
            <form>
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="title">Заголовок:</label>
                    <input type="text" name="title" class="form-control" placeholder="Заголовок" value="{{ isset($post) ? $post->title : old('title') }}" id="title">
                </div>
                <div class="form-group">
                    <label for="content">Содержание:</label>
                    <textarea name="post" id="post">{{ isset($post) ? $post->post : old('post') }}</textarea>
                </div>

                <div class="form-group">
                    @if (isset($post))
                        <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/post/' . $post->id . '/update') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Сохранить изменения">
                            <i class="far fa-save fa-fw"></i> Сохранить
                        </button>
                    @else
                        <button type="submit" class="btn btn-outline-success shadow" formaction="{{ url('/adminpanel/post/store') }}" formmethod="POST" data-toggle="tooltip" data-placement="top" title="Создать новую запись">
                            <i class="fas fa-check fa-fw"></i> Создать
                        </button>
                    @endif

                    <a type="submit" class="btn btn-outline-danger shadow" href="{{ url('/adminpanel') }}" data-toggle="tooltip" data-placement="top" title="Вернуться на предыдущую страницу">
                        <i class="fas fa-times fa-fw"></i> Отменить
                    </a>
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            {!! $error !!}<br>
                        @endforeach

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if (session('message'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {!! session('message') !!}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

            </form>
        </div>
    </div>
@endsection

@section('customJs')
    <script src="{{ url('assets/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/ckeditor/lang/ru.js') }}"></script>

    <script>
        CKEDITOR.replace( 'post', {
            // language: 'ru',
            uiColor: '#e6f9ff',

            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] },
                { name: 'about', groups: [ 'about' ] }
            ],

            removeButtons: 'Save,NewPage,Preview,Print,PasteFromWord,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Templates,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,PageBreak,Iframe,About',

            extraPlugins: 'uploadimage',
            filebrowserUploadUrl: '/adminpanel/image/upload',
            // filebrowserBrowseUrl: '/images/browse',

            disableNativeSpellChecker: false,
            removePlugins: 'liststyle,tabletools,tableselection,scayt,wsc,contextmenu'
        });
    </script>
@endsection
