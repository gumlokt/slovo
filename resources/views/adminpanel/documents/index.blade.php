@extends('adminpanel.layouts.app')

@section('content')
    <div class="row border-bottom mb-4">
        <div class="col-8 col-sm-8">
            <h3 class="text-danger">
                <i class="far fa-file-alt fa-fw"></i> Документация
            </h3>
        </div>

        <div class="col-4 col-sm-4 text-right">
            @if (1 == Auth::user()->id)
                <button class="btn btn-outline-primary btn-sm dropdown-toggle shadow" type="button" data-toggle="collapse" data-target="#collapseUploadFile" aria-expanded="false" aria-controls="collapseUploadFile" title="Открыть форму загрузки файла на сервер">
                    <i class="fas fa-plus fa-fw"></i>
                </button>
            @endif
        </div>
    </div>





    @if (1 == Auth::user()->id)
        <div class="row mb-4">
            <div class="col">
                <div class="collapse" id="collapseUploadFile">
                    <div class="card">

                        <div class="card-header">
                            Отправка файла
                        </div>

                        <form class="card-body" action="{{ url('/adminpanel/documents/upload') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12 col-lg-7 col-xl-8">
                                    <div class="input-group">
                                        <div class="input-group-prepend shadow">
                                            <span class="input-group-text" id="fileDescription">Описание:</span>
                                        </div>
                                        <input type="text" class="form-control" name="description" placeholder="Введите описание для прикрепляемого файла" aria-label="Username" aria-describedby="fileDescription">
                                    </div>
                                </div>

                                <div class="col-12 col-lg-5 col-xl-4">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="document" aria-describedby="fileUploadAddon" id="fileUpload">
                                            <label class="custom-file-label" for="fileUpload">Выберите файл</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-outline-secondary shadow" data-toggle="tooltip" data-placement="top" title="Отправить файл" id="fileUploadAddon">
                                                <i class="fas fa-upload fa-fw"></i> Отправить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    @endif



    <div class="row mb-4" id="files">
        <div class="col" v-if="files">
            <table class="table table-hover">
                <tbody>
                    <tr v-for="(file, index) in files" v-bind:class="[curIndex == index ? 'table-warning' : '']">
                        <td>
                            <span v-bind:class="[curIndex == index ? 'text-danger' : '']">@{{ file.description }}</span>
                        </td>
                        <td>
                            <i v-bind:class="getExtension(index)"></i>
                            <span v-bind:class="[curIndex == index ? 'text-danger' : '']">@{{ file.name }}</span>
                        </td>
                        <td class="text-right">
                            <a class="btn btn-outline-success btn-sm shadow" v-bind:href="'/adminpanel/downloadFile/' + file.name" data-toggle="tooltip" data-placement="top" title="Скачать">
                                <i class="fas fa-download fa-fw"></i> Скачать файл
                            </a>
                            <!-- <button class="btn btn-outline-success btn-sm shadow" type="button" data-toggle="tooltip" data-placement="top" title="Скачать" v-on:click="downloadFile(index)">
                                <i class="fas fa-download fa-fw"></i> Скачать
                            </button> -->
                            <button class="btn btn-outline-danger btn-sm shadow" type="submit" data-toggle="tooltip" data-placement="top" title="Удалить" v-if="1 == uid" v-on:click="deleteFile(index)" v-bind:disabled="btnStatus">
                                <i class="fa fa-times fa-fw"></i> Удалить
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection



@section('customJs')
    <script>
        var app = new Vue({
            el: '#files',
            data: {
                uid: {{ Auth::user()->id }},
                admin: {{ Auth::user()->admin }},
                animateDumping: false,
                btnStatus: false,
                files: null,
                curIndex: -1
            },
            methods: {
                displayFiles: function () {
                    axios.get('/adminpanel/displayFiles').then(response => {
                        this.files = response.data.files;
                        this.curIndex = -1;
                        this.btnStatus = false;
                        console.log(response.data);
                    }).catch(error => {
                        console.log(error);
                    });
                },
                // downloadFile: function (index) {
                //     const url = '/adminpanel/downloadFile/' + this.files[index].name;
                //     const link = document.createElement('a');
                //     link.href = url;
                //     link.setAttribute('download', this.files[index].name); //or any other extension
                //     document.body.appendChild(link);
                //     link.click();
                // },
                deleteFile: function (index) {
                    this.btnStatus = true;
                    this.curIndex = index;

                    axios.post('/adminpanel/deleteFile', {
                        fileName: this.files[index].name
                    }).then(response => {
                        this.displayFiles();
                        console.log(response.data);
                    }).catch(error => {
                        this.displayFiles();
                        console.log(error);
                    });
                },
                getExtension: function (index) {
                    return 'far fa-file fa-fw';
                }
            },
            computed: {
            },
            created: function () {
                this.displayFiles();
            }
        })
    </script>
@endsection
