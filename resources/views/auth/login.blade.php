@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-4">
        <div class="col-12">
            <div class="media">
                <img src="{{ asset('images/logo.png') }}" width="20%" class="mr-4" alt="logo">
                <div class="media-body">

                    <div class="row alert alert-info" style="padding: 1em; opacity: 0.95; border-radius: 4em 1em;">
                        <div class="col">
                            <blockquote class="blockquote text-center">
                                <h4 class="" style="text-shadow: 0 1px 0 #eee;">&nbsp; Прибыль превыше всего, но <cite><strong>Честь</strong></cite> превыше прибыли &nbsp;</h4>
                            </blockquote>
                        </div>
                    </div>

                    <div class="row alert alert-info" style="padding: 1em; opacity: 0.95; border-radius: 4em 1em;">
                        <div class="col">
                            <blockquote class="blockquote text-center">
                                <h4 class="" style="text-shadow: 0 1px 0 #eee;"><strong>Уважаемы коммерсанты!</strong></h4>
                                <p>
                                    Приглашаем Вас вступить в товарищество <strong>«Слово Купеческое»</strong>
                                </p>
                            </blockquote>
                            
                            <blockquote class="blockquote text-left">
                                Требование для вступления:
                                <ol>
                                    <li>Наличие коммерческое деятельности не менее 1 года</li>
                                    <li>Принимаются только резиденты Российской Федерации</li>
                                    <li>Безупречная репутация (проверка по программе контур фокус)</li>
                                </ol>
                            </blockquote>

                            <blockquote class="blockquote text-left">
                                Вам Будет предложено:
                                <ol>
                                    <li><a href="{{ url('downloadFile/2019-06-07-11-07-38.pdf') }}">Устав</a> товарищества</li>
                                    <li>Страхование сделок</li>
                                    <li>Касса взаимопомощи</li>
                                    <li>Форум сообщества</li>
                                    <li>Выдача свидетельства участника</li>
                                    <li>Информационная поддержка</li>
                                    <li>и многое другое...</li>
                                </ol>
                                <p>При положительном решении, жмите на ссылку <strong class="text-danger"><i class="far fa-handshake fa-fw"></i> Вступить</strong> в верхнем меню сайта</p>
                            </blockquote>
                        </div>
                    </div>

                </div>
            </div>

            <transition name="fade">
                <div class="card bg-light" style="position: absolute; top: 0em; right: 0em; z-index: 100;" v-if="showLoginForm">
                    <div class="card-header"><i class="far fa-user"></i> Вход в личый кабинет</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">
                                    <i class="far fa-envelope"></i> {{ __('E-Mail') }}
                                </label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ваш E-Mail" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">
                                    <i class="fas fa-key"></i> Пароль
                                </label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Пароль" autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            Запомнить меня
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-outline-primary">
                                        <i class="fas fa-sign-in-alt"></i> Войти
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Забыли пароль?
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </transition>

            <transition name="fade">
                <div class="card bg-light" style="position: absolute; top: 0em; right: 0em; z-index: 100;" v-if="showMembershipRequestForm">
                    <div class="card-header"><i class="far fa-handshake fa-fw"></i> Запрос на вступление в СНТ</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('membership') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">
                                    <i class="far fa-user fa-fw"></i> ФИО
                                </label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Ваши ФИО" v-model="contact.name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inn" class="col-md-4 col-form-label text-md-right">
                                    <i class="far fa-id-card fa-fw"></i> ИНН
                                </label>

                                <div class="col-md-8">
                                    <input id="inn" type="text" class="form-control @error('inn') is-invalid @enderror" name="inn" value="{{ old('inn') }}" required autocomplete="inn" placeholder="Ваш ИНН" v-model="contact.inn">

                                    @error('inn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">
                                    <i class="fas fa-mobile-alt fa-fw"></i> Тел.
                                </label>

                                <div class="col-md-8">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" placeholder="Ваш телефон" v-model="contact.phone">

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">
                                    <i class="far fa-envelope"></i> {{ __('E-Mail') }}
                                </label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Ваш E-Mail" v-model="contact.email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-outline-primary" v-bind:disabled="btnDisabled" v-on:click.prevent="sendMembershipRequest()">
                                        <i class="fas fa-check fa-fw"></i> Отправить
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row mb-0 mt-2">
                                <div class="col">
                                    <div class="alert alert-danger" role="alert" style="opacity: 0.8;" v-show="statusMessage">
                                        <span v-html="statusMessage"></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </transition>

        </div>
    </div>
</div>
@endsection

@section('customJs')
        <script src="{{ url('assets/axios.min.js') }}"></script>
        <script src="{{ url('assets/vue.min.js') }}"></script>
        <script>
            var logIn = new Vue({
                el: '#app',

                data: {
                    showLoginForm: false,
                    showMembershipRequestForm: false,
                    contact: {
                        name: '',
                        inn: '',
                        phone: '',
                        email: ''
                    },
                    btnDisabled: false,
                    statusMessage: ''
                },

                methods: {
                    sendMembershipRequest: function () {
                        this.statusMessage = '';

                        if (this.contact.name && this.contact.phone) {
                            this.btnDisabled = true;

                            axios.post("membership/get", this.contact).then(response => {
                                // set all properties to empty string: ''
                                Object.keys(this.contact).forEach(k => this.contact[k] = '');
                                // this.btnDisabled = false;
                                this.statusMessage = 'Ваша заявка успешно отправлена...';
                                setTimeout(() => { 
                                    this.btnDisabled = false;
                                    this.showMembershipRequestForm = false;
                                } , 1500);
                            });
                        } else {
                            this.statusMessage = 'Поля <strong>ФИО</strong> и <strong>Тел.</strong> обязательны для заполнения';
                        }
                    }
                }
            });
        </script>
@endsection
