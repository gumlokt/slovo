<!DOCTYPE html>
<html>
<head>
    <title>Запрос на вступление в товарищество</title>
</head>
<body>
    <h3>Это письмо содержит контактные данные лица, желающего вступить в Содружество независимых товарищей «Слово Купеческое»</h3>

    <p>ФИО: {{ $myLetter->name }}</p>
    <p>ИНН: {{ $myLetter->inn }}</p>
    <p>Тел.: {{ $myLetter->phone }}</p>
    <p>E-Mail: {{ $myLetter->email }}</p>
    <br>
    <hr>
    <p>С уважением, <br> администрация сайта http://slovo89.ru</p>
</body>
</html>
