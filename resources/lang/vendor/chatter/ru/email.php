<?php

return [
    'preheader'       => 'Just wanted to let you know that someone has responded to a forum post.',
    'greeting'        => 'Всем Привет,',
    'body'            => 'Just wanted to let you know that someone has responded to a forum post at',
    'view_discussion' => 'Посмотреть Обсуждение',
    'farewell'        => 'Доброго Времени Суток!',
    'unsuscribe'      => [
        'message' => 'If you no longer wish to be notified when someone responds to this form post be sure to uncheck the notification setting at the bottom of the page :)',
        'action'  => 'Don\'t like these emails?',
        'link'    => 'Отписаться от этого обсуждения',
    ],
];
