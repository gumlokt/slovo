<?php

return [
    'success' => [
        'title'  => 'Успешно!',
        'reason' => [
            'submitted_to_post'       => 'Ваш комментарий к обсуждению размещен.',
            'updated_post'            => 'Обсуждение успешно отредактировано.',
            'destroy_post'            => 'Successfully deleted the response and discussion.',
            'destroy_from_discussion' => 'Комментарий к обсуждению успешно удален.',
            'created_discussion'      => 'Новое обсуждение успешно создано.',
        ],
    ],
    'info' => [
        'title' => 'Инфо!',
    ],
    'warning' => [
        'title' => 'Внимание!',
    ],
    'danger'  => [
        'title'  => 'Упссс...!',
        'reason' => [
            'errors'            => 'Пожалуйста, исправьте следующие ошибки:',
            'prevent_spam'      => 'In order to prevent spam, please allow at least :minutes in between submitting content.',
            'trouble'           => 'Sorry, there seems to have been a problem submitting your response.',
            'update_post'       => 'Nah ah ah... Could not update your response. Make sure you\'re not doing anything shady.',
            'destroy_post'      => 'Nah ah ah... Could not delete the response. Make sure you\'re not doing anything shady.',
            'create_discussion' => 'Уппссс :( Похоже, произошла какая-то при попытке создания обсуждения.',

        	'title_required'    => 'Введите наименование заголовка обсуждения',
        	'title_min'		    => 'Длина заголовка должна быть не менее :min символов.',
        	'title_max'		    => 'Длина заголовка должна быть не более :max символов.',
        	'content_required'  => 'Введите текст обсуждения',
        	'content_min'  		=> 'Содержимое должно быть не менее :min символов.',
        	'category_required' => 'Выберите категорию.',
        ],
    ],
];
