<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return redirect('login');
});

Route::post('membership/get', 'MembershipController@membership')->name('membership');
Route::get('downloadFile/{fileName}', 'MembershipController@downloadFile')->name('downloadFile');



Auth::routes();

Route::group(['prefix' => 'adminpanel', 'middleware' => 'auth'], function () {
    // Documents --------------------------------------------------------------
    Route::get('documents', 'DocumentController@index');
    Route::post('documents/upload', 'DocumentController@upload');

    Route::get('displayFiles', 'DocumentController@displayFiles');
    Route::get('downloadFile/{fileName}', 'DocumentController@downloadFile');
    Route::post('deleteFile', 'DocumentController@deleteFile');


    // Posts (start page) -----------------------------------------------------
    Route::get('', 'IndexController@index');

    Route::get('post/create', 'IndexController@create');
    Route::post('post/store', 'IndexController@store');

    Route::get('post/{id}/show', 'IndexController@show');

    Route::get('post/{id}/edit', 'IndexController@edit');
    Route::post('post/{id}/update', 'IndexController@update');

    Route::get('post/{id}/delete', 'IndexController@delete');
    Route::post('post/{id}/destroy', 'IndexController@destroy');

    // Route::get('images/browse', 'IndexController@browse');
    Route::post('image/upload', 'IndexController@upload');



    // Polls ------------------------------------------------------------------
    Route::get('polls', 'PollController@index');
    Route::get('polls/archives', 'PollController@archives');

    Route::get('poll/create', 'PollController@create');
    Route::post('poll/store', 'PollController@store');

    Route::get('poll/{id}/show', 'PollController@show');

    Route::get('poll/{id}/pause', 'PollController@pause');
    Route::get('poll/{id}/activate', 'PollController@activate');

    Route::get('poll/{id}/edit', 'PollController@edit');
    Route::post('poll/{id}/update', 'PollController@update');

    Route::get('poll/{id}/archive', 'PollController@archive');
    Route::get('poll/{id}/restore', 'PollController@restore');

    Route::post('poll/{id}/vote', 'PollController@vote');
    Route::get('poll/{id}/voteRemove', 'PollController@voteRemove');

    Route::get('poll/{id}/delete', 'PollController@delete');
    Route::post('poll/{id}/destroy', 'PollController@destroy');


    Route::post('polls/all', 'PollController@all');




    // Mailing ----------------------------------------------------------------
    Route::get('mailings', 'MailingController@index');

    // Route::get('message/create', 'MessageController@create');
    // Route::post('message/store', 'MessageController@store');

    Route::get('letter/create', 'LetterController@create');
    Route::post('letter/store', 'LetterController@store');




    // Conversation -----------------------------------------------------------
    Route::get('conversations', 'ConversationController@index');
    Route::get('contacts', 'ContactsController@get');
    Route::get('conversation/{id}', 'ContactsController@getMessagesFor');
    Route::post('conversation/send', 'ContactsController@send');





    // Manage users -----------------------------------------------------------
    Route::get('users', 'UserController@index');

    Route::get('user/create', 'UserController@create');
    Route::post('user/store', 'UserController@store');

    Route::get('user/{id}/pause', 'UserController@pause');
    Route::get('user/{id}/activate', 'UserController@activate');

    Route::get('user/{id}/edit', 'UserController@edit');
    Route::post('user/{id}/update', 'UserController@update');

    Route::get('user/{id}/delete', 'UserController@delete');
    Route::post('user/{id}/destroy', 'UserController@destroy');

    Route::get('user/{id}/reinvite', 'UserController@reinvite');



    // User profile -----------------------------------------------------------
    Route::get('profile', 'ProfileController@index');
    Route::get('profile/{id}/edit', 'UserController@edit');
    Route::post('profile/{id}/update', 'ProfileController@update');

    Route::get('avatar/get/{id}', 'ProfileController@getAvatar');
    Route::post('avatar/set', 'ProfileController@setAvatar');
    Route::get('avatar/delete/{id}', 'UserController@deleteAvatar');



    // --- AJAX requests ------------------------------------------------------
    // Cateogry create
    Route::post('forums/category/create', 'CategoryController@create');
});
