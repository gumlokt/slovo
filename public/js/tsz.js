// jQuery
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#addAnswer').click(function (event) {
        event.preventDefault();
        $('#answers').append(`
            <div class="row mb-2">
                <div class="col-10">
                    <input type="text" name="answers[]" class="form-control" placeholder="Вариант ответа">
                </div>
                <button class="btn btn-outline-danger btn-sm shadow" onclick="$($(this).parent()).remove()" data-toggle="tooltip" data-placement="top" title="Удалить этот вариант ответа">
                    <i class="fas fa-minus fa-fw fa-xs"></i>
                </button>
            </div>
        `);
    });
});
