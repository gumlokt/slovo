<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'apartment'     => '42',
            'name'          => 'Admin',
            'admin'         => '1',
            'active'        => '1',
            'email' 		=> 'tsz89@yandex.ru',
            'password' 		=> bcrypt('111111'),
            'created_at' 	=> strftime('%F %T'), // %F - same as Y-m-d 
            'updated_at' 	=> strftime('%F %T'), // %T - same as H:i:s 
        ]);
    }
}
