<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Letter;

class SendLetter extends Mailable {
    use Queueable, SerializesModels;

    public $myLetter;

    /**
     * Create a new letter instance.
     *
     * @return void
     */
    public function __construct(Letter $letter) {
        $this->myLetter = $letter;
    }

    /**
     * Build the letter.
     *
     * @return $this
     */
    public function build() {
        return $this->view('emails.sendletter')->subject($this->myLetter->subject);
    }
}
