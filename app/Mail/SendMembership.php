<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Http\Request;

class SendMembership extends Mailable {
    use Queueable, SerializesModels;

    public $myLetter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->myLetter = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->view('emails.sendmembership')->subject('Запрос на вступление в товарищество');
    }
}
