<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'poll',
    ];


    public function answers() {
        return $this->hasMany('App\Answer');
    }

    public function choices() {
        return $this->hasMany('App\Choice');
    }

    public function results() {
        return $this->hasMany('App\Result');
    }



}
