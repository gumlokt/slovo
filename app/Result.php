<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'poll_id', 'answer_id', 'votes', 'total',
    ];


    public function polls() {
        return $this->belongsTo('App\Poll');
    }



}
