<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'poll_id', 'answer',
    ];


    public function polls() {
        return $this->belongsTo('App\Poll');
    }

    public function choices() {
        return $this->hasMany('App\Choice');
    }


}
