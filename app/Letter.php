<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'subject', 'letter',
    ];


    public function mailings() {
        return $this->hasMany('App\Mailing');
    }



}
