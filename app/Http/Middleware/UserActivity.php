<?php

namespace App\Http\Middleware;

use Closure;

use Carbon\Carbon;
use Auth;
use Cache;

use App\User;
use DB;

class UserActivity {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(Auth::check()) {
            $expiresAt = Carbon::now()->addMinutes(2);

            // update time in cache
            Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);

            // update time in DB
            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update(['last' => Carbon::now()]);
        }

        return $next($request);
    }
}
