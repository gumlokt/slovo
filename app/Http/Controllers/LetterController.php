<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;

use DB;
use App\User;
use App\Letter;
use App\Mailing;
use App\Mail\SendLetter;


class LetterController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }


    public function create() {
    	$users = User::orderBy(DB::raw('cast(apartment as unsigned)'))->get();

        return view('adminpanel.letters.create', ['users' => $users]);
    }


    public function store(Request $request) {
        $this->validate($request, [
            'email' => 'array',
            'subject' => 'required',
            'letter' => 'required',
        ]);


        $letter = Letter::create([ 'user_id' => Auth::user()->id, 'subject' => $request->input('subject'), 'letter' => $request->input('letter'), ]);

        foreach ($request->input('checkedEmails') as $checkedEmail) {
        	$user = User::where('email', $checkedEmail)->first();

        	Mailing::create([ 'user_id' => $user->id, 'letter_id' => $letter->id, ]);

            Mail::to($user)->send(new SendLetter($letter));
        }


        return redirect('/adminpanel/mailings');
    }


}
