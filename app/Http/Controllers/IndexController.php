<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;



use Auth;
use App\Post;

class IndexController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }





    public function index() {
    	$posts = Post::orderBy('created_at', 'DESC')->simplePaginate(5);

        return view('adminpanel.index', ['posts' => $posts]);
    }




    public function create() {
        return view('adminpanel.posts.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'post'      => 'required',
        ]);


        $sql = [
            'user_id' 	=> Auth::user()->id,
            'title' 	=> $request->input('title'),
            'post'      => $request->input('post'),
        ];

        $post = Post::create($sql);


        return redirect('/adminpanel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $post = Post::find($id);


        return view('adminpanel.posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $post = Post::find($id);


        return view('adminpanel.posts.create', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required|max:255',
            'post'      => 'required',
        ]);



        $sql = [
            'title' => $request->input('title'),
            'post'      => $request->input('post'),
        ];


        $post = Post::find($id);
        $post->update($sql);


        return redirect('/adminpanel');
    }

    public function delete($id) {
        $post = Post::find($id);


        return view('adminpanel.posts.confirmation', ['post' => $post]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Post::destroy($id);


        return redirect('/adminpanel');
    }











    // public function browse(Request $request) {
    //     $type = $request->input('CKEditor');

    //     return $type;
    // }

    public function upload(Request $request) {
        if ($request->hasFile('upload')) {
            // Storage path is: storage/public/images (do not forget to make 'php artisan storage:link')
            $storePath = 'public/images/';
            $fileName = 'image_' . date('Y-m-d--H-i-s'). '.' . $request->file('upload')->extension();


            $request->file('upload')->storeAs($storePath, $fileName);

            $response = [
                'uploaded'  => 1,
                'fileName'  => $fileName,
                'url'       => '/storage/images/' . $fileName,
                'error'     => ["message" => $storePath],
            ];
        } else {
            $response = [
                'uploaded'  => 0,
                'error'     => ['message' => 'Some error is ocured...Request had nod image...'],
            ];
        }


        return json_encode($response);
    }


}
