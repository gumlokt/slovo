<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\AppProfileController;
use Auth;

use DB;
use App\User;
use App\Mail\SendPassword;


use App\Choice;
use App\Letter;
use App\Mailing;
use App\Post;



class UserController extends Controller {
    // Display a list of all users
    public function index() {
        // if (1 == Auth::user()->id) {
            $users = User::orderBy(DB::raw('cast(apartment as unsigned)'))->get();

            return view('adminpanel.users.index', ['users' => $users]);
        // }


        // return redirect('/adminpanel/profile');
    }


    // Show the form for creating a new user
    public function create() {
        if (1 == Auth::user()->id) {
            return view('adminpanel.users.create');
        }


        return redirect('/adminpanel/profile');
    }


    // Store a newly created user in storage
    public function store(Request $request) {
        if (1 == Auth::user()->id) {
            $this->validate($request, [
                'apartment' => 'required|max:255',
                'name'      => 'required|max:255',
                'admin'     => 'required|max:255',

                'email'     => 'required|email|max:255|unique:users',
                'password'  => 'required|min:4|confirmed',

                'city'      => 'max:255',
                'gender'    => 'max:255',
                'year'      => 'max:4',
                // 'aboutme'   => 'min:10',
            ]);


            $sql = [
                'apartment' => $request->input('apartment'),
                'name'      => $request->input('name'),
                'admin'     => $request->input('admin'),

                'active'    => 1,
                'email'     => $request->input('email'),
                'password'  => bcrypt($request->input('password')),

                'city'      => $request->input('city'),
                'gender'    => $request->input('gender'),
                'year'      => $request->input('year'),
                'aboutme'   => $request->input('aboutme'),
            ];

            $user = User::create($sql);

            Mail::to($user)->send(new SendPassword($user, $request->input('password')));

            return redirect('/adminpanel/users');
        }


        return redirect('/adminpanel/profile');
    }


    // Display the specified user
    public function show($id) {
        if (1 == Auth::user()->id) {
            //
        }


        return redirect('/adminpanel/profile');
    }


    // Show the form for editing the specified user.
    public function edit($id) {
        // if (1 == Auth::user()->id) {
            $user = User::find($id);
            return view('adminpanel.users.create', ['user' => $user]);
        // }

        // return redirect('/adminpanel/profile');
    }


    // Update user data
    public function update(Request $request, $id) {
        if (1 == Auth::user()->id) {
            $this->validate($request, [
                'apartment' => 'required|max:255',
                'name'      => 'required|max:255',
                'admin'     => 'required|max:255',
                // 'email'     => 'required|email|max:255|unique:users',

                'city'      => 'max:255',
                'gender'    => 'max:255',
                'year'      => 'max:4',
                // 'aboutme'   => 'min:10',
            ]);


            $sql = [
                'apartment' => $request->input('apartment'),
                'name'      => $request->input('name'),
                'admin'     => $request->input('admin'),
                'email'     => $request->input('email'),

                'city'      => $request->input('city'),
                'gender'    => $request->input('gender'),
                'year'      => $request->input('year'),
                'aboutme'   => $request->input('aboutme'),
            ];



            $user = User::find($id);


            if (!empty($request->input('email')) and $request->input('email') != $user->email) {
                $this->validate($request, [
                    'email'     => 'required|email|max:255|unique:users',
                ]);

                $sql = array_add($sql, 'email', $request->input('email'));
            }


            if (!empty($request->input('password')) or !empty($request->input('password_confirmation'))) {
                $this->validate($request, [
                    'password'  => 'required|min:6|confirmed',
                ]);

                $sql = array_add($sql, 'password', bcrypt($request->input('password')));
            }

            // $user = User::find($id);
            $user->update($sql);

            return redirect('/adminpanel/users');
        }


        return redirect('/adminpanel/profile');
    }



    // Display user delete confirmation
    public function delete($id) {
        if (1 == Auth::user()->id) {
            $user = User::find($id);
            return view('adminpanel.users.confirmation', ['user' => $user]);
        }


        return redirect('/adminpanel/profile');
    }



    // Destroy user
    public function destroy($id) {
        if (1 == Auth::user()->id) {
            if (1 != $id) {
                User::destroy($id);
                Choice::where('user_id', '=', $id)->delete();
                Letter::where('user_id', '=', $id)->delete();
                Mailing::where('user_id', '=', $id)->delete();
                Post::where('user_id', '=', $id)->delete();
                $this->deleteAvatar($id);
            }
        }


        return redirect('/adminpanel/users');
    }



    // Reinvite user - send invite message again
    public function reinvite($id) {
        if (1 == Auth::user()->id) {
            if (1 != $id) {
                // First generate new password for user
                // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $characters = '0123456789';
                $charactersLength = strlen($characters);
                $password = '';

                for ($i = 0; $i < 4; $i++) {
                    $password .= $characters[rand(0, $charactersLength - 1)];
                }


                $user = User::find($id);
                $user->password = bcrypt($password);
                $user->save();

                Mail::to($user)->send(new SendPassword($user, $password));
            }
        }


        return redirect('/adminpanel/users');
    }





    // Pause user
    public function pause($id) {
        if (1 == Auth::user()->id) {
            User::where('id', '=', $id)->update(['active' => 0]);
            return redirect('/adminpanel/users');
        }


        return redirect('/adminpanel/profile');
    }



    // Activate user
    public function activate($id) {
        if (1 == Auth::user()->id) {
            User::where('id', '=', $id)->update(['active' => 1]);

            return redirect('/adminpanel/users');
        }


        return redirect('/adminpanel/profile');
    }





    // Remove user's avatar
    public function deleteAvatar($id) {
        $paths = Storage::files('avatars');

        foreach ($paths as $path) {
            // $path looks something like this: 'avatars/filename.ext'
            $fileName = substr(strstr($path, '/'), 1);
            $name = strstr($fileName, '.', true);

            if ($name == $id) {
                Storage::delete('avatars/' . $fileName);

                return json_encode([ 'result' => true, 'message' => 'Avatar was successfully deleted...' ]);
            }
        }


        return json_encode([ 'result' => false, 'message' => 'Avatar was not deleted...' ]);
    }








}
