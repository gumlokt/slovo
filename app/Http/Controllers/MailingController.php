<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;
use App\Letter;


class MailingController extends Controller {
    public $emails = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {
        $users = User::all();
        foreach ($users as $user) {
            $this->emails[$user->id] = $user->email;
        }

        $letters = Letter::orderBy('created_at', 'DESC')->paginate(5);

        return view('adminpanel.mailings.index', [ 'emails' => $this->emails, 'letters' => $letters, ]);
    }


}
