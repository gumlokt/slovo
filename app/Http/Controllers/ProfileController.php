<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;

use Auth;

class ProfileController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }





    public function index() {
        return view('adminpanel.profile.index');
    }



    public function update(Request $request, $id) {
        $this->validate($request, [
            // 'apartment' => 'required|max:255',
            'name'      => 'required|max:255',
            'admin'     => 'required|max:255',
            // 'email'     => 'required|email|max:255|unique:users',

            'city'      => 'max:255',
            'gender'    => 'max:255',
            'year'      => 'max:4',
            // 'aboutme'   => 'min:10',
        ]);


        $sql = [
            // 'apartment' => $request->input('apartment'),
            'name'      => $request->input('name'),
            'admin'     => $request->input('admin'),
            // 'email'     => $request->input('email'),

            'city'      => $request->input('city'),
            'gender'    => $request->input('gender'),
            'year'      => $request->input('year'),
            'aboutme'   => $request->input('aboutme'),
        ];



        $user = User::find($id);

        if (1 == Auth::user()->id) {
            $this->validate($request, [
                'apartment' => 'required|max:255',
            ]);

            $sql = array_add($sql, 'apartment', $request->input('apartment'));
        }


        if (!empty($request->input('email')) and $request->input('email') != $user->email) {
            $this->validate($request, [
                'email'     => 'required|email|max:255|unique:users',
            ]);

            $sql = array_add($sql, 'email', $request->input('email'));
        }


        if (!empty($request->input('password')) or !empty($request->input('password_confirmation'))) {
            $this->validate($request, [
                'password'  => 'required|min:6|confirmed',
            ]);

            $sql = array_add($sql, 'password', bcrypt($request->input('password')));
        }

        // $user = User::find($id);
        $user->update($sql);

        return redirect('/adminpanel/profile');
    }


    public function show(Request $request) {
        $user = User::find($request->input('id'));

        if ($user) {
            return $user->name;
        }

        return 0;
    }








    // Get user's avatar by ID
    public function getAvatar($id) {
        $paths = Storage::files('avatars');

        foreach ($paths as $path) {
            // $path looks something like this: 'avatars/filename.ext'
            $fileName = substr(strstr($path, '/'), 1);
            $name = strstr($fileName, '.', true);

            if ($name == $id) {
                return Storage::get('avatars/' . $fileName);
            }
        }


        return Storage::get('avatars/default.png');
    }


    // Set user's avatar
    public function setAvatar(Request $request) {
        if ($request->hasFile('avatar')) {
            // First, delete old avatar image if exists
            $paths = Storage::files('avatars');
            foreach ($paths as $path) {
                // $path looks something like this: 'avatars/filename.ext'
                $fileName = substr(strstr($path, '/'), 1);
                $name = strstr($fileName, '.', true);

                if ($name == Auth::user()->id) {
                    Storage::delete('avatars/' . $fileName);
                }
            }

            // Storage path is: storage/public/images (do not forget to make 'php artisan storage:link')
            $storePath = 'avatars/';
            $fileName = Auth::user()->id . '.' . $request->file('avatar')->extension();


            $request->file('avatar')->storeAs($storePath, $fileName);

            return json_encode([ 'result' => true, 'message' => 'Avatar was successfully uploaded...' ]);
        }


        return json_encode([ 'result' => false, 'message' => 'Avatar was not uploaded...' ]);
    }









}
