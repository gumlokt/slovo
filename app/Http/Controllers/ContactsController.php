<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use DB;

use App\User;
use App\Message;
use App\Events\NewMessage;

class ContactsController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }


    public function get() {
        $contacts = User::where('id', '!=', auth()->id())->get();

        $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
        ->where('to', auth()->id())
        ->where('read', false)
        ->groupBy('from')
        ->get();

        $contacts = $contacts->map(function($contact) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();

            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $contact;
        });


        return response()->json($contacts);
    }

    public function getMessagesFor($id) {
        // $messages = Message::where('from', $id)->orWhere('to', $id)->get();
        Message::where('from', $id)->where('to', auth()->id())->update(['read' => true]);

        $messages = Message::where(function($query) use ($id) {
            $query->where('from', auth()->id());
            $query->where('to', $id);
        })->orWhere(function($query) use ($id) {
            $query->where('from', $id);
            $query->where('to', auth()->id());
        })->get();

        return response()->json($messages);
    }


    public function send(Request $request) {
        $message = Message::create([
            'from' => auth()->id(),
            'to' => $request->input('contact_id'),
            'text' => $request->input('text')
        ]);

        broadcast(new NewMessage($message));
        return response()->json($message);
    }



}
