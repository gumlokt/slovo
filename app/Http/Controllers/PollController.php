<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Poll;
use App\Answer;
use App\Choice;
use App\Result;

class PollController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $polls = Poll::where('archive', '=', 0)->orderBy('created_at', 'DESC')->paginate(5);

        $choices = [];
        foreach ($polls as $poll) {
            $choice = Choice::where('user_id', '=', Auth::user()->id)
                ->where('poll_id', '=', $poll->id)
                ->get();

            if (!empty($choice[0])) {
                $choices[$poll->id] = $choice[0]->answer_id;
            }
        }

        return view('adminpanel.polls.index', ['polls' => $polls, 'choices' => $choices]);
    }

    public function archives() {
        $polls = Poll::where('archive', '=', 1)->orderBy('created_at', 'DESC')->paginate(5);

        $archives = [];

        foreach ($polls as $poll) {
            $results = Result::where('poll_id', '=', $poll->id)->get();
            $totalVotes = 0;
            foreach ($results as $result) {
                $archives[$poll->id][$result->answer_id] = $result->votes;
                $totalVotes += $result->votes;
            }
            $archives[$poll->id]['totalVotes'] = $totalVotes;


            $choice = Choice::where('user_id', '=', Auth::user()->id)->where('poll_id', '=', $poll->id)->get();
            if (!empty($choice[0])) {
                $archives[$poll->id]['myChoice'] = $choice[0]->answer_id;
            }
        }


        return view('adminpanel.polls.archives', [ 'polls' => $polls, 'archives' => $archives, ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('adminpanel.polls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'poll' => 'required|max:255',
            'answers.*' => 'required',
        ]);



        $poll = Poll::create([ 'poll' => $request->input('poll'), 'active' => 1, ]);

        $answers = $request->input('answers');
        foreach ($answers as $answer) {
            if ($answer) {
                Answer::create([ 'poll_id' => $poll->id, 'answer' => $answer, ]);
            }
        }




        return redirect('/adminpanel/polls');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $poll = Poll::find($id);

        $answers = Answer::where('poll_id', '=', $poll->id)->get();
        $results = [];
        foreach ($answers as $answer) {
            $results[$answer->id] = 0;
        }

        $choices = Choice::where('poll_id', '=', $poll->id)->get();
        // $countChoices = count($choices);
        $myChoice = '';
        foreach ($choices as $choice) {
            $results[$choice->answer_id]++;
            if ($choice->user_id == Auth::user()->id) {
                $myChoice = $choice->answer_id;
            }
        }



        return view('adminpanel.polls.show', ['poll' => $poll, 'answers' => $answers, 'myChoice' => $myChoice, 'results' => $results]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $poll = Poll::find($id);
        $answers = Answer::where('poll_id', '=', $poll->id)->get();


        return view('adminpanel.polls.create', ['poll' => $poll, 'answers' => $answers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'poll' => 'required|max:255',
        ]);



        $poll = Poll::find($id);
        $poll->update([ 'poll' => $request->input('poll'), ]);

        $answers = Answer::where('poll_id', '=', $poll->id)->get();
        $countAnswers = count($answers);

        $newAnswers = array_diff($request->input('answers'), array('')); // delete empty elements
        $countNewAnswers = count($newAnswers);


        if ($countNewAnswers > $countAnswers) {
            for ($i = 0; $i < $countNewAnswers; $i++) {
                if (isset($answers[$i])) {
                    $answers[$i]->update([ 'answer' => $newAnswers[$i], ]);
                } else {
                    Answer::create([ 'poll_id' => $poll->id, 'answer' => $newAnswers[$i], ]);
                }
            }
        }

        if ($countNewAnswers < $countAnswers) {
            for ($i = 0; $i < $countAnswers; $i++) {
                if (isset($newAnswers[$i])) {
                    $answers[$i]->update([ 'answer' => $newAnswers[$i], ]);
                } else {
                    $answers[$i]->delete();
                    Choice::where('poll_id', '=', $id)->where('answer_id', '=', $answers[$i]->id)->delete();
                }
            }
        }

        if ($countNewAnswers == $countAnswers) {
            for ($i = 0; $i < $countAnswers; $i++) { 
                $answers[$i]->update([ 'answer' => $newAnswers[$i], ]);
            }
        }





        return redirect('/adminpanel/polls');
    }

    public function delete($id) {
        $poll = Poll::find($id);

        return view('adminpanel.polls.confirmation', ['poll' => $poll]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (1 == Auth::user()->id) {
            Choice::where('poll_id', '=', $id)->delete();
            Result::where('poll_id', '=', $id)->delete();
            Answer::where('poll_id', '=', $id)->delete();
            Poll::destroy($id);
        }


        return redirect('/adminpanel/polls');
    }







    public function pause($id) {
        Poll::where('id', '=', $id)->update(['active' => 0]);

        return redirect('/adminpanel/polls');
    }

    public function activate($id) {
        Poll::where('id', '=', $id)->update(['active' => 1]);

        return redirect('/adminpanel/polls');
    }

    public function archive($id) {
        Poll::where('id', '=', $id)->update(['active' => 0, 'archive' => 1]);


        $answers = Answer::where('poll_id', '=', $id)->get();
        $votes = [];
        foreach ($answers as $answer) {
            $votes[$answer->id] = 0;
        }


        $choices = Choice::where('poll_id', '=', $id)->get();
        foreach ($choices as $choice) {
            $votes[$choice->answer_id]++;
        }


        foreach ($votes as $key => $value) {
            Result::create(['poll_id' => $id, 'answer_id' => $key, 'votes' => $value]);
        }


        return redirect('/adminpanel/polls');
    }

    public function restore($id) {
        Poll::where('id', '=', $id)->update(['active' => 1, 'archive' => 0]);
        Result::where('poll_id', '=', $id)->delete();


        return redirect('/adminpanel/polls/archives');
    }


    public function vote(Request $request, $id) {
        if ($request->input('choice')) {
            $sql = [
                'user_id'   => Auth::user()->id,
                'poll_id'   => $id,
                'answer_id'   => $request->input('choice'),
            ];

            $choice = Choice::where('user_id', '=', Auth::user()->id)->where('poll_id', '=', $id)->get();

            if (isset($choice[0])) {
                $choice[0]->update($sql);
            } else {
                Choice::create($sql);
            }
        }


        return redirect('/adminpanel/polls');
    }


    public function voteRemove($id) {
        $choice = Choice::where('user_id', '=', Auth::user()->id)->where('poll_id', '=', $id)->get();

        if (isset($choice[0])) {
            $choice[0]->delete();
        }


        return redirect('/adminpanel/polls');
    }



        public function all(Request $request) {
            $polls = Poll::where('poll', 'LIKE', $request->input('term') . '%')->orderBy('created_at', 'DESC')->get();


            return json_encode($polls);
        }

}
