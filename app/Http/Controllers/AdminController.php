<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }





    public function index() {
        return view('adminpanel.index');
    }




    public function create() {
        return view('adminpanel.posts.create');
    }




}
