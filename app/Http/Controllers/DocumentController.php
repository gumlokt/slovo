<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Document;
class DocumentController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }



    public function index() {
        return view('adminpanel.documents.index');
    }


    public function upload(Request $request) {
        if ($request->hasFile('document')) {
            $fileName = date('Y-m-d-H-i-s') . '.' . $request->file('document')->extension();
            $sql = [ 'description' => $request->input('description'), 'name' => $fileName ];

            $document = Document::create($sql);
            $request->file('document')->storeAs('documents', $fileName);
        }

        return redirect('adminpanel/documents');
    }



    public function displayFiles() {
        $files = Document::orderBy('created_at', 'DESC')->get();

        return [ 'files' => $files ];
    }


    // download selected file
    public function downloadFile($fileName) {
        return Storage::download('documents/' . $fileName);
    }


    // delete selected file
    public function deleteFile(Request $request) {
        sleep(1);
        $fileName = $request->input('fileName');

        Storage::delete('documents/' . $fileName);
        Document::where('name', '=', $fileName)->delete();

        return [ 'message' => 'Выбранный бэкап базы данных успешно удалён.' ];
    }

}
