<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Chatter_category;

class CategoryController extends Controller {

    public function create(Request $request) {
        $sql = [
            'name'      => $request->input('newCategoryName'),
            'color'     => strtoupper('#' . dechex(rand(40, 255)) . dechex(rand(40, 255)) . dechex(rand(40, 255))),
            'slug'      => strtolower($request->input('newCategoryName')),
        ];

        $category = Chatter_category::create($sql);


        return redirect('/adminpanel/forums');
    }




}
