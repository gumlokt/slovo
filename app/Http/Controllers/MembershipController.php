<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMembership;
use App\User;

class MembershipController extends Controller {
    public function membership(Request $request) {
        sleep(1);

        $user = User::where('id', 1)->first();
        Mail::to($user)->send(new SendMembership($request));
        
        return $request;
    }


    // download selected file
    public function downloadFile($fileName) {
        return Storage::download('documents/' . $fileName);
    }



}
