<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'poll_id', 'answer_id',
    ];





    public function polls() {
        return $this->belongsTo('App\Poll');
    }

    public function answers() {
        return $this->belongsTo('App\Answer');
    }



}
