<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'letter_id',
    ];





    public function letters() {
        return $this->belongsTo('App\Letter');
    }

    public function users() {
        return $this->belongsTo('App\User');
    }

}
